# Forest Rave [GameBoy]

"Animation meme" / demo, ft. Fluffbutt-Mishu

Original [meme](https://www.youtube.com/watch?v=JxsRS4_AFJA)

## Notes
* XCF/      : Source files for art (GIMP .xcf and Krita .kra files)
* include/  : Constants, macros
* sound/    : Main sound engine (using a modified version of [GBMP v2](https://github.com/ZoomTen/gbmp-v2))
* gfx/      : Raw graphics which are then converted to GameBoy format
* scenes/   : ASM code for individual scenes (code as well as subroutines

Stage graphics are taken from Super Mario Land 2.

Some [inspiration](https://pixelation.org/index.php?topic=15404.0) for the Pokémon-type battle graphics

## Dependencies
* [RGBDS](https://github.com/rednex/rgbds/releases)
* GNU make
* GNU coreutils

## Build [Windows]
* Download [win-bash](https://sourceforge.net/projects/win-bash/files/shell-complete/latest/). This will also cause the `GNU coreutils` and `GNU make` to become available.
* Download RGBDS. The package should be suffixed with `-win32` or `-win64` depending on your architecture (32/64 bit). You may need [7zip](http://www.7-zip.org/download.html) to open those.
* Click on the download button in this repository -> Download ZIP.
* Extract the ZIP file somewhere.
* Extract the contents of the `win-bash` package in the `win` directory (Create the directory if it doesn't exist for some reason)
* Extract the contents of the `win32` folder of the RGBASM package under the same directory
* Run `compile.bat`.

## Build [Linux]
These instructions are for Ubuntu (Also Lubuntu, Kubuntu and all the other -buntus. Tested in 16.04)

* Open terminal of choice and copy-paste away...
* Install essential tools:
    * `sudo apt-get install make git`
* Install RGBDS:
    * `sudo apt-get install byacc flex pkg-config libpng-dev`
    * `git clone https://github.com/rednex/rgbds ~/rgbds`
    * `cd ~/rgbds; make; sudo make install`
* Build project:
    * `git clone https://gitlab.com/zumid/forest-rave-gb.git ~/forestrave`
    * `cd ~/forestrave; make`

For other Linux distributions, consult the package managers for each package, as they might be named differently.