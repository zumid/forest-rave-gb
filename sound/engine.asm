BEAT	equ	$20

UpdateMusic:
	ld a, [w_song_enabled]
	and a
	ret z
	ld a, [w_song_speed]
	ld c, a
	ld a, [w_song_timer]
	add a, c
	ld [w_song_timer], a
	jr nc, .do_FX				; not the time?
.do_notes
	call Music_A_ParseNotes
	call Music_B_ParseNotes
	call Music_C_ParseNotes
	call Music_D_ParseNotes
; extra features to count song bar
	ld a, [w_song_tick]
	inc a
	ld [w_song_tick], a
	cp a, $10
	jr nz, .do_FX
.countbeat
	xor a
	ld [w_song_tick], a
	ld a, [w_song_beat]
	inc a
	ld [w_song_beat], a
	cp a, 2
	jr nz, .do_FX
.countbar
	xor a
	ld [w_song_beat], a
	ld a, [w_song_position]
	inc a
	ld [w_song_position], a
.do_FX
	call Music_A_ApplyFX	; Then do instrument playback
	call Music_B_ApplyFX
	call Music_C_ApplyFX
	call Music_D_Drums
.return
	ret

CommonFX_SetOutput:
	inc de
	ld a, [de]
	ld [rNR51], a
	inc de
	ret
	
CommonFX_GetNote:	
; where a = note value
	ld b, 0
	ld c, a
	ld hl, NotePitches
	add hl, bc
	add hl, bc
	ret
	
CommonFX_GetDuty:
	rlca
	rlca
	rlca
	rlca
	rlca			; because duty is on bits 6 & 7
	rlca			; usually 0-3 duty
	ret

include	"sound/engine/engine.a.asm"
include	"sound/engine/engine.b.asm"
include	"sound/engine/engine.c.asm"
include	"sound/engine/engine.d.asm"

include	"sound/tables/pitches.asm"
include	"sound/tables/instruments.asm"
include	"sound/tables/vibrato.asm"
include	"sound/tables/waves.asm"
include	"sound/tables/drums.asm"

