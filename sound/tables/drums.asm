DrumTable:
	dw .empty	;0
	dw .hat		;1
	dw .kick	;2
	dw .snare	;3
	
.empty
	db $00, $00, $01
	db $FF
	dw .empty
.hat
; format = Note, envelope, length
	db $31, $41, $21
.hat_
	db $00, $00, $01
	db $FF			; loop from this position
	dw .hat_
	
.kick
	db $32,$90,00
	db $34,$80,00
	db $43,$71,03
.kick_
	db $00, $00, $01
	db $FF
	dw .kick_

.snare
	db $32,$90,01
	db $34,$80,00
	db $32,$74,08
.snare_
	db $00, $00, $01
	db $FF
	dw .snare_