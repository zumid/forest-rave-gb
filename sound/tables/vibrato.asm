VibratoTable:			; blech.,
	dw .v020			; 01
	dw .v040			; 02
	
.v020
	db 0, 0, 0, 0		; delay 4 frames
.v020_
	db -1, -2, -2, -1, 0, 1, 2, 2, 1	; delta from original pitch
	db $80								; looping byte
	dw .v020_

.v040
	db 0, -1
	db $80
	dw .v040