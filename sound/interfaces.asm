
Music_Play:
	di
	ld hl, Music_SongList
	ld c, a
	ld b, 0
	
	add hl, bc
	add hl, bc
	add hl, bc
	
	ld a, [hli]
	ld [w_song_speed], a

	ld a, [hli]
	ld e, a
	ld a, [hl]
	ld d, a

	ld b, d
	ld c, e
	ld hl, w_ch1_address

	ld c, 8
.loadchannels
	ld a, [de]
	inc de
	ld [hli], a
	dec c
	jr nz, .loadchannels

	xor a
	inc a
	ld [w_song_enabled], a
	ld a, $FF
	ld [rNR50], a
	reti