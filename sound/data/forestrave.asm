FORESTRAVETR equ $01	; Note transpose

Mus_ForestRave:
	dw .ch1		; Channel 1 [Sqr1] pointer
	dw .ch2		; Channel 2 [Sqr2] pointer
	dw .ch3		; Channel 3 [Wave] pointer
	dw .ch4		; Channel 4 [Nois] pointer

.ch4
	kickstartSound
	m_drum 0, 128	; rest
	m_drum 0, 128	; rest
.ch4_
	goLoop .ch4_kicks, 12-1
	goLoop .ch4_kicks, 8-1
	goLoop .ch4_kicks, 4-1
	goLoop .ch4_kickssnares, 8-1
	goEnd
.ch4_kicks
	m_drum 2, 4
	m_drum 1, 2
	m_drum 1, 2
	m_drum 2, 4
	m_drum 1, 2
	m_drum 1, 2
	m_drum 2, 4
	m_drum 1, 2
	m_drum 1, 2
	m_drum 2, 4
	m_drum 1, 2
	m_drum 1, 2
	goReturn
.ch4_kickssnares
	m_drum 2, 4
	m_drum 1, 2
	m_drum 1, 2
	m_drum 3, 4
	m_drum 1, 2
	m_drum 1, 2
	m_drum 2, 4
	m_drum 1, 2
	m_drum 1, 2
	m_drum 3, 4
	m_drum 1, 2
	m_drum 1, 2
	goReturn
	
.ch1
	useInstrument 0
	setDuty 2
	setEnvelope 13, 1
	kickstartSound
	m_note rs, 1, 128
	m_note rs, 1, 128
	goLoop .ch1_kicks, 11-1
	goCall .ch1_kicks2
	goLoop .ch1_kicks, 11-1
	goCall .ch1_kicks2
	goLoop .ch1_kicks, 5-1
	goCall .ch1_kicks2
	setEnvelope 4, 0
	setVibrato 1
	setSweep 0
	m_note rs, 1, 1
	setTranspose FORESTRAVETR-12
	m_note C_, 5, 8*2-1
	m_note F_, 4, 4*2
	m_note G_, 4, 4*2
	m_note G#, 4, 8*2
	m_note F_, 4, 2*2
	m_note D#, 4, 2*2
	m_note F_, 4, 2*2
	m_note G_, 4, 2*2
	m_note A_, 4, 8*3*2
	setEnvelope 0, 0
	m_note rs, 1, 1
	goEnd
.ch1_kicks
	setSweep %00011011
	m_note	C_,	6,	8
	m_note	C_,	6,	8
	m_note	C_,	6,	8
	m_note	C_,	6,	8
	goReturn
.ch1_kicks2
	setSweep %00011011
	m_note	C_,	6,	8
	m_note	C_,	6,	8
	m_note	C_,	6,	8
	m_note	C_,	6,	2
	m_note	C_,	6,	2
	m_note	C_,	6,	2
	m_note	C_,	6,	2
	goReturn

.ch2
	setEnvelope 8, 2
	kickstartSound
	useInstrument 0
	setVibrato 1
	m_note rs, 1, 128	;1 empty bar
	m_note rs, 1, 128	;1 empty bar
	m_note rs, 1, 128	;1 empty bar
	m_note rs, 1, 128	;1 empty bar
	setTranspose FORESTRAVETR-24
	goCall .ch2_pattern8
	setTranspose FORESTRAVETR-12
	goCall .ch3_pattern1 ; substituet pattern ,. 3
	goCall .ch3_pattern1 ; substituet pattern ,. 3
	setDuty 3
	useInstrument 0
	setTranspose FORESTRAVETR-12
	setEnvelope 6, 1
	goCall .ch2_pattern7
	setEnvelope 8, 2
	setTranspose FORESTRAVETR+5-24
	goCall .ch3_pattern1 ; substituet pattern ,. 4
	setTranspose FORESTRAVETR-24
	goCall .ch2_pattern5
	goEnd
.ch2_pattern5
	m_note F_, 4, 2
	m_note G_, 4, 2
	m_note G#, 4, 2
	m_note A#, 4, 2
	m_note C_, 5, 2
	m_note rs, 1, 2
	m_note F_, 5, 2
	m_note rs, 1, 2
	m_note G#, 5, 4
	m_note G_, 5, 2
	m_note F_, 5, 2
	m_note D#, 5, 2
	m_note rs, 1, 2
	m_note C_, 5, 2
	m_note rs, 1, 2
	m_note F_, 5, 2
	m_note rs, 1, 2
	m_note D#, 5, 2
	m_note C#, 5, 2
	m_note C_, 5, 2
	m_note rs, 1, 2
	m_note A#, 4, 2
	m_note rs, 1, 2
	m_note G#, 4, 2
	m_note rs, 1, 2
	m_note G_, 4, 2
	m_note rs, 1, 2
	m_note G#, 4, 2
	m_note rs, 1, 2
	m_note A#, 4, 2
	m_note rs, 1, 2
	useInstrument 0
	setTranspose FORESTRAVETR-12
	m_note F_, 4, 2
	m_note G_, 4, 2
	m_note G#, 4, 2
	m_note A#, 4, 2
	m_note C_, 5, 2
	m_note rs, 1, 2
	m_note F_, 5, 2
	m_note rs, 1, 2
	m_note G#, 5, 4
	m_note G_, 5, 2
	m_note F_, 5, 2
	m_note D#, 5, 2
	m_note rs, 1, 2
	m_note C_, 5, 2
	m_note rs, 1, 2
	m_note F_, 5, 4
	m_note D#, 5, 2
	m_note C#, 5, 2
	m_note C_, 5, 2
	m_note rs, 1, 2
	m_note A#, 4, 2
	m_note rs, 1, 2
	m_note G#, 4, 4
	m_note G_, 4, 4
	m_note G#, 4, 4
	m_note A#, 4, 4
	m_note F_, 4, 2
	m_note G_, 4, 2
	m_note G#, 4, 2
	m_note A#, 4, 2
	m_note C_, 5, 2
	m_note rs, 1, 2
	useInstrument 5
	m_note F_, 4, 2
	m_note rs, 1, 2
	useInstrument 0
	m_note F_, 4, 2
	m_note G_, 4, 2
	m_note G#, 4, 2
	m_note A#, 4, 2
	m_note C_, 5, 2
	m_note rs, 1, 2
	useInstrument 5
	m_note F_, 4, 2
	m_note rs, 1, 2
	useInstrument 0
	m_note F_, 4, 2
	m_note G_, 4, 2
	m_note G#, 4, 2
	m_note A#, 4, 2
	m_note C_, 5, 2
	m_note rs, 1, 2
	useInstrument 5
	m_note F_, 4, 2
	m_note rs, 1, 2
	useInstrument 0
	goReturn
.ch2_pattern7
	m_note C_, 5, 2
	m_note D#, 5, 2
	m_note G_, 5, 2
	m_note C_, 6, 2
	m_note C_, 5, 2
	m_note D#, 5, 2
	m_note G_, 5, 2
	m_note C_, 6, 2
	m_note C_, 5, 2
	m_note D#, 5, 2
	m_note G_, 5, 2
	m_note C_, 6, 2
	m_note C_, 5, 2
	m_note D#, 5, 2
	m_note G_, 5, 2
	m_note C_, 6, 2
	m_note A#, 4, 2
	m_note C#, 5, 2
	m_note F_, 5, 2
	m_note A#, 5, 2
	m_note A#, 4, 2
	m_note C#, 5, 2
	m_note F_, 5, 2
	m_note A#, 5, 2
	m_note A#, 4, 2
	m_note C#, 5, 2
	m_note F_, 5, 2
	m_note A#, 5, 2
	m_note A#, 4, 2
	m_note C#, 5, 2
	m_note F_, 5, 2
	m_note A#, 5, 2
	m_note E_, 5, 2
	m_note F_, 5, 2
	m_note G_, 5, 2
	m_note C_, 6, 2
	m_note E_, 5, 2
	m_note F_, 5, 2
	m_note G_, 5, 2
	m_note C_, 6, 2
	m_note E_, 5, 2
	m_note F_, 5, 2
	m_note G_, 5, 2
	m_note C_, 6, 2
	m_note E_, 5, 2
	m_note F_, 5, 2
	m_note G_, 5, 2
	m_note C_, 6, 2
	m_note E_, 5, 2
	m_note F_, 5, 2
	m_note G_, 5, 2
	m_note C_, 6, 2
	m_note E_, 5, 2
	m_note F_, 5, 2
	m_note G_, 5, 2
	m_note C_, 6, 2
	m_note E_, 5, 2
	m_note F_, 5, 2
	m_note G_, 5, 2
	m_note C_, 6, 2
	useInstrument 5
	m_note C_, 6, 2
	m_note A#, 5, 2
	m_note G#, 5, 2
	m_note G_, 5, 2
	goReturn
.ch2_pattern8
	setDuty 3
	m_note C_, 5, 2
	m_note D_, 5, 2
	m_note D#, 5, 2
	m_note F_, 5, 2
	m_note G_, 5, 2
	m_note rs, 1, 2
	m_note C_, 6, 2
	m_note rs, 1, 2
	m_note A#, 5, 2
	m_note rs, 1, 2
	m_note G_, 5, 2
	m_note rs, 1, 2
	m_note A_, 5, 2
	m_note rs, 1, 2*3
	m_note C_, 5, 2
	m_note D_, 5, 2
	m_note D#, 5, 2
	m_note F_, 5, 2
	m_note G_, 5, 2
	m_note rs, 1, 2
	m_note C_, 6, 2
	m_note rs, 1, 2
	m_note A#, 5, 2
	m_note rs, 1, 2
	m_note G_, 5, 2
	m_note rs, 1, 2
	m_note A_, 5, 2
	m_note rs, 1, 2*3
	m_note C_, 5, 2
	m_note D_, 5, 2
	m_note D#, 5, 2
	m_note F_, 5, 2
	m_note G_, 5, 2
	m_note rs, 1, 2
	m_note C_, 6, 2
	m_note rs, 1, 2
	m_note C_, 5, 2
	m_note D_, 5, 2
	m_note D#, 5, 2
	m_note F_, 5, 2
	m_note G_, 5, 2
	m_note rs, 1, 2
	m_note C_, 6, 2
	m_note rs, 1, 2
	m_note C_, 5, 2
	m_note D_, 5, 2
	m_note D#, 5, 2
	m_note F_, 5, 2
	m_note C_, 5, 2
	m_note D_, 5, 2
	m_note D#, 5, 2
	m_note F_, 5, 2
	m_note C_, 5, 2
	m_note D_, 5, 2
	m_note D#, 5, 2
	m_note F_, 5, 2
	useInstrument 5
	m_note G_, 5, 2
	m_note F_, 5, 2
	m_note D#, 5, 2
	m_note D_, 5, 2
	goReturn

.ch3
	setOutput %11111111
	setEnvelope 0, 1
	kickstartSound
	setTranspose FORESTRAVETR
	setWave 1
	useInstrument 0
	goCall .ch3_pattern1
	goCall .ch3_patternglitch
	goCall .ch3_pattern9
	setTranspose FORESTRAVETR
	goCall .ch3_pattern10
	goCall .ch3_pattern10
	goCall .ch3_pattern10
	goCall .ch3_pattern10
	goCall .ch3_pattern13
	setTranspose FORESTRAVETR + 5
	goCall .ch3_pattern10
	goCall .ch3_pattern10
	setTranspose FORESTRAVETR ; pattern 12
;	goCall .ch3_pattern12
	m_note F_, 2, 4 * 8
	m_note F_, 1, 4 * 8
	setEnvelope 0, 3
	setWave 3
	m_note rs, 1, 1
	goEnd
.ch3_pattern1
	m_note C_, 4, 2
	m_note D_, 4, 2
	m_note D#, 4, 2
	m_note F_, 4, 2
	m_note G_, 4, 2
	m_note rs, 1, 2
	m_note C_, 5, 2
	m_note rs, 1, 2
	m_note A#, 4, 2
	m_note rs, 1, 2
	m_note G_, 4, 2
	m_note rs, 1, 2
	m_note A_, 4, 2
	m_note rs, 1, 2*3
	m_note A#, 4, 2
	m_note rs, 1, 2
	m_note G_, 4, 2
	m_note A_, 4, 2
	m_note A#, 4, 2
	m_note rs, 1, 2
	m_note A_, 4, 2
	m_note F_, 4, 2
	m_note D_, 4, 2
	m_note rs, 1, 2
	m_note D#, 4, 2
	m_note D_, 4, 1
	m_note D#, 4, 1
	m_note D_, 4, 2
	m_note rs, 1, 2*3
	m_note C_, 4, 2
	m_note D_, 4, 2
	m_note D#, 4, 2
	m_note F_, 4, 2
	m_note D#, 4, 2
	m_note rs, 1, 2
	m_note D_, 4, 2
	m_note D#, 4, 2
	m_note F_, 4, 2
	m_note rs, 1, 2
	m_note D#, 4, 2
	m_note D_, 4, 2
	m_note C_, 4, 2
	m_note rs, 1, 2*3
	m_note F_, 4, 2
	m_note rs, 1, 2
	m_note D#, 4, 2
	m_note D_, 4, 2
	m_note C_, 4, 2
	m_note rs, 1, 2
	m_note D_, 4, 2
	m_note D#, 4, 2
	m_note F_, 4, 2
	m_note rs, 1, 2
	m_note D#, 4, 2
	m_note F_, 4, 2
	m_note G_, 4, 2
	m_note rs, 1, 2*3
	goReturn
.ch3_patternglitch
	m_note C_, 4, 2
	m_note D_, 4, 2
	m_note D#, 4, 2
	m_note F_, 4, 2
	m_note G_, 4, 2
	m_note rs, 1, 2
	m_note C_, 5, 4 ;;;;;;;
	m_note A#, 4, 1 ;;;;;;;
	m_note C_, 6, 1 ;;;;;;;
	m_note rs, 1, 1 ;;;;;;;
	m_note C_, 6, 1 ;;;;;;;
	m_note G_, 4, 2
	m_note rs, 1, 2
	m_note A_, 4, 2
	m_note rs, 1, 2*3
	m_note A#, 4, 2
	m_note rs, 1, 2
	m_note G_, 4, 2
	m_note A_, 4, 2
	m_note A#, 4, 2
	m_note rs, 1, 2
	m_note A_, 4, 2
	m_note F_, 4, 2
	m_note D_, 4, 2
	m_note rs, 1, 2
	m_note D#, 4, 2
	m_note D_, 4, 1
	m_note D#, 4, 1
	m_note D_, 4, 1
	m_note rs, 1, 1
	m_note D_, 4, 1
	m_note rs, 1, 1
	m_note D_, 4, 1
	m_note rs, 1, 1
	m_note D_, 4, 1
	m_note rs, 1, 1
	m_note C_, 4, 2
	m_note D_, 4, 2
	m_note D#, 4, 2
	m_note F_, 4, 2
	m_note D#, 4, 2
	m_note rs, 1, 2
	m_note D_, 4, 2
	m_note D#, 4, 2
	m_note F_, 4, 2
	m_note rs, 1, 2
	m_note D#, 4, 2
	m_note D_, 4, 2
	m_note C_, 4, 2
	m_note rs, 1, 2*3
	m_note F_, 4, 2
	m_note rs, 1, 2
	m_note D#, 4, 2
	m_note D_, 4, 2
	m_note C_, 4, 2
	m_note rs, 1, 2
	m_note D_, 4, 2
	m_note D#, 4, 2
	m_note F_, 4, 1
	m_note rs, 1, 1
	m_note F_, 4, 1
	m_note rs, 1, 1
	m_note D#, 4, 2
	m_note F_, 4, 2
	m_note G_, 4, 2
	m_note rs, 1, 1
	m_note G_, 4, 1
	m_slide G#, 4, 1
	m_slide A_, 4, 1
	m_slide A#, 4, 1
	m_slide B_, 4, 1
	goReturn
.ch3_pattern9
	setWave 2
	useInstrument 6
	setTranspose -12 + FORESTRAVETR
	m_note C_, 2, 4
	m_note C_, 3, 4
	m_note C_, 2, 4
	m_note C_, 3, 4
	m_note C_, 2, 4
	m_note C_, 3, 4
	m_note C_, 2, 4
	m_note C_, 3, 4
	m_note C_, 2, 4
	m_note C_, 3, 4
	m_note C_, 2, 4
	m_note C_, 3, 4
	m_note C_, 2, 4
	m_note C_, 3, 4
	m_note C_, 2, 4
	m_note C_, 3, 4
	m_note C_, 2, 4
	m_note C_, 3, 4
	m_note C_, 2, 4
	m_note C_, 3, 4
	m_note C_, 2, 4
	m_note C_, 3, 4
	m_note C_, 2, 4
	m_note C_, 3, 4
	m_note C_, 2, 4
	m_note C_, 3, 4
	m_note C_, 2, 4
	m_note C_, 3, 4
	m_note C_, 2, 4
	m_note C_, 3, 4
	m_note C_, 2, 4
	m_note C_, 3, 4
	goReturn
.ch3_pattern10
	m_note C_, 1, 4
	m_note C_, 2, 4
	m_note C_, 1, 4
	m_note C_, 2, 4
	m_note C_, 1, 4
	m_note C_, 2, 4
	m_note C_, 1, 4
	m_note F_, 2, 4
	m_note A#, 1, 4
	m_note A#, 2, 4
	m_note A#, 1, 4
	m_note A#, 2, 4
	m_note A#, 1, 4
	m_note A#, 2, 4
	m_note A#, 1, 4
	m_note A#, 2, 4
	m_note G#, 1, 4
	m_note G#, 2, 4
	m_note G#, 1, 4
	m_note G#, 2, 4
	m_note G#, 1, 4
	m_note G#, 2, 4
	m_note G#, 1, 4
	m_note G#, 2, 4
	m_note A#, 1, 4
	m_note A#, 2, 4
	m_note A#, 1, 4
	m_note A#, 2, 4
	m_note A#, 1, 4
	m_note A#, 2, 4
	m_note A#, 1, 4
	m_note B_, 2, 4
	goReturn
.ch3_pattern11
	goReturn
.ch3_pattern12
	goReturn
.ch3_pattern13
	m_note C_, 2, 4
	m_note C_, 3, 4
	m_note C_, 2, 4
	m_note C_, 3, 4
	m_note C_, 2, 4
	m_note C_, 3, 4
	m_note C_, 2, 4
	m_note C_, 3, 4
	m_note A#, 1, 4
	m_note A#, 2, 4
	m_note A#, 1, 4
	m_note A#, 2, 4
	m_note A#, 1, 4
	m_note A#, 2, 4
	m_note A#, 1, 4
	m_note B_, 2, 4
	m_note C_, 2, 4
	m_note C_, 3, 4
	m_note C_, 2, 4
	m_note C_, 3, 4
	m_note C_, 2, 4
	m_note C_, 3, 4
	m_note C_, 2, 4
	m_note C_, 3, 4
	m_note C_, 2, 4
	m_note C_, 3, 4
	m_note C_, 2, 4
	m_note C_, 3, 4
	m_note C_, 2, 4
	m_note C_, 3, 4
	m_note C_, 2, 4
	m_note C_, 3, 4
	goReturn