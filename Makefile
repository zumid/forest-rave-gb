ROMNAME := forest_rave
GFXDIR = gfx
GFXSRC = $(shell find $(GFXDIR) -name *.png)
GFXOUT = $(patsubst $(GFXDIR)/%.png, $(GFXDIR)/%.2bpp, $(GFXSRC))

all: gb

gb: $(ROMNAME).gb

$(ROMNAME).gb: $(ROMNAME).o
	rgblink -n $(ROMNAME).sym -o $(ROMNAME).gb $(ROMNAME).o
	rgbfix -v $(ROMNAME).gb
	rm $<

$(ROMNAME).o: $(ROMNAME).asm $(GFXOUT) scenes/*
	rgbasm -h -o $(ROMNAME).o $(ROMNAME).asm

$(GFXDIR)/%.2bpp: $(GFXDIR)/%.png
	rgbgfx -o $@ $<
	
clean:
	rm gfx/*.2bpp
	rm *.gb *.sym