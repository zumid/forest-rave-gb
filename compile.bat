@echo off

if exist win\make.exe (
	echo Make exists
	if exist win\rgb*.exe (
		echo RGBDS exists
		echo Proceeding with compilation...
		echo === === === ===
		win\make.exe
		echo === === === ===
	) else (
		echo RGBDS doesn't exist! Please consult README.md
	)
) else (
	echo Make doesn't exist! Please consult README.md
)
pause