; ---------------------------------------------------------------------------
; Special functions
; ---------------------------------------------------------------------------

GenerateMap:
; Generates a image tilemap and puts in BGMap
; a = start tile
; d = image Y size (in tiles)
; e = image X size (in tiles)
	push de
.p
	push af
	call WaitVRAM
	pop af
	ld [hli], a
	inc a
	dec e
	jr nz, .p
	pop de
	ld c, e
	push de
.q
	dec hl
	dec c
	jr nz, .q
	ld bc, $0020
	add hl, bc
	pop de
	dec d
	push de
	jr nz, .p
	pop de
	ret
	
ParseMap:
; A parse function using the format described below
; hl = dest.
; de = loc.
	push hl
	ld a, [de] ; read column
	ld bc, 0
	ld c, a
	add hl, bc
	inc de
	ld a, [de]	; read row
	push de		; de is counter now
	ld d, a
	xor a
	ld e, $20
.one
	add e
	jr nc, .onek
	inc b
.onek
	dec d
	jr nz, .one
	ld c, a
	xor a
	add hl, bc
	pop de
	inc de
	ld a, [de]	; read data length
	ld c, a
.two
	inc de
	ld a, [de]
	push af
	call WaitVRAM
	pop af
	ld [hli], a
	dec c
	jr nz, .two
	inc de
	ld a, [de]
	cp a, $80
	pop hl
	ret z		; return if == $FF
	jr ParseMap
	
ParseText:
; Parses a text-map
; hl = dest.
; de = loc.
	ld c, 0				; keep track of charas?
.loop
	ld a, [de]
	cp "#"				; check for newline
	jr nz, .noline	
	inc de
	ld a, [de]
.pew
	dec hl 
	dec c
	jr nz, .pew
	ld bc, $20
	add hl, bc
	ld c, 0
.noline
	cp $5B				; check for end character
	ret z
	ld b, a
	call WaitVRAM
	ld a, b
	ld [hli], a
	inc de
	inc c
	jr .loop

TypeText:
; Types in a text-map
; hl = dest
; de = src
	ld a, [de]	; dang dirty text dialog
	cp $7F
	jr z, .x
	cp $FF
	ret z
	jr .continue
.x
	dec hl
	dec b
	jr nz, .x
	ld bc, $0020
	add hl, bc
	add hl, bc
	inc de
	jr TypeText
.continue
	push af
	call WaitVRAM
	pop af
	ld [hl], a
	inc hl
	inc de
	inc b
	call DelayFrame
	call DelayFrame
	jr TypeText

BlinkArrow:
; Blinks an arrow on the screen (if battle tiles are loaded)
; hl = arrow position in BGmap
	call WaitVRAM
	ld a, $1A		; arrow tile
	ld [hl], a
	ld c, 6
	call DelayFrames
	call WaitVRAM
	ld a, $00		; blank tile
	ld [hl], a
	ld c, 6
	call DelayFrames
	dec b
	jr nz, BlinkArrow
	ret
	
GenerateOAM:
; hl = location (spriteRAM!!!)
; a  = starting tile
; bc = y-size and x-size
; de = starting position (y, x)

; this looks dangerous
	push af
	ld a, 8
.x
	inc d
	inc e
	dec a
	jr nz, .x
	pop af
	push bc
	push de
	push af
.y
; move to #4
	inc hl
	inc hl
	inc hl
	xor a
	ld [hl], a
; move back to #0
	dec hl
	dec hl
	dec hl
; add entries
	ld a, d
	ld [hli], a
	ld a, e
	ld [hli], a
	pop af
	ld [hli], a
	inc a
	push af
	inc hl
; X position
	inc e
	inc e
	inc e
	inc e
	inc e
	inc e
	inc e
	inc e
	dec c
	jr nz, .y
	inc sp
	inc sp
	pop de
	inc d
	inc d
	inc d
	inc d
	inc d
	inc d
	inc d
	inc d
	push de
	inc sp
	inc sp
	pop bc
	dec b
	push bc
	inc sp
	inc sp
	ret z
	dec sp
	dec sp
	dec sp
	dec sp
	dec sp
	dec sp
	jr .y
