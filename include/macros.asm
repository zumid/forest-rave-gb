oam_def: MACRO
; x,y,tile,flags
	db \2 * 8 + 8
	db \1 * 8 + 8 - 2	; the "-2" is to compensate
						; for the battle screen
						; shitty i know
	db \3
	db \4
ENDM

loadVRAM: MACRO
; Dest, Src, Size
	ld hl, \1
	ld de, \2
	ld bc, \3
	call CopyVRAM
ENDM

waitY: MACRO
; wait for some scanline
	ld d, \1
	call CheckLine
ENDM

fill: MACRO
; fill an area of RAM
; Address, Byte, How many
	ld hl, \1
	ld bc, \3
	ld a, \2
	call FillRAM
ENDM

fillVRAM: MACRO
; fill an area of RAM
; Address, Byte, How many
	ld hl, \1
	ld bc, \3
	ld a, \2
	call FillVRAM
ENDM