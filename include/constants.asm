; Mishu sprite size
SPRITESIZE_X1		equ	4
SPRITESIZE_Y1		equ	7

; Scene tick numbers
INTRO_TICK			equ 0
GLITCH_TICK_A		equ 4 ;4,1. 5,1
GLITCH_TICK_B		equ 5
PLAYERSEL_TICK		equ 8
STAGE1_TICK			equ 12
STAGE2_TICK			equ 20
MAPHELL_TICK		equ 28
TRAINING_TICK		equ 32
FINAL_TICK			equ 36
ENDSCREEN_TICK		equ	42

PS_CHARAMAX			equ	2	; max. chara. count

; Boss constants
m_HP_Enemy			equ vBGMap0 + ($20 * 2) + 4
m_HP_Player			equ vBGMap0 + ($20 * 10) + 12

; Sprite location
a_Sprites	equ		$D000

; HRAM
H_VBF		equ		$FFD6
H_TEST		equ		$FFD7
H_DMA		equ 	$FF80

t_Rock		equ		$c
