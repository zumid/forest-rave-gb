; === Main Variables ===

SECTION "Forest Rave Vars", WRAM0[$CE00]
; --- Parallax scrolling ---
v_Scrollx::			ds 1
v_Scrollx2::		ds 1
; --- Counter to limit scrolling ---
v_ScrollTimer::		ds 1
v_ScrollTimer2::	ds 1
; --- How many frames in sprite ---
v_SpriteTimer::		ds 1
v_CurSprite::		ds 1
; --- Player Select vars ---
v_PS_CharaCount::	ds 1
; --- Stage vars ---
v_MovementIndex::	ds 1	; Sprite movement no.
v_MovValue::		ds 1	; Movement delta
v_MovCounter::		ds 1	; Movement repeats left
v_MovDirection::	ds 1	; 0=Y, 1+=X
v_SpeedFlag::		ds 1	; causes ground to move at 2x speed
; --- Battle vars ---
v_Transition_Stage::ds 1
v_HP_Enemy::		ds 1
v_HP_Player::		ds 1
v_SineCounter::		ds 1
; --- Global scene counter ---
v_CurScene::		ds 1
v_CurrentKP::		ds 1	; Keypress