; ---------------------------------------------------------------------------
; Engine Functions
; ---------------------------------------------------------------------------

; Functions from Pokemon Red/Blue
DelayFrame:
; *The* step function
; No parameters
	ld a, 1						; let us know that we're checking for V-blank
	ld [H_VBF], a
.halt
	halt
	ld a, [H_VBF]
	and a						; Hit V-blank?
	jr nz, .halt				; if not, keep waiting
	ret

Delay6:
; Delay 6 frames
; No parameters
	call DelayFrame
	call DelayFrame
	call DelayFrame
Delay3:
; Delay 3 frames
; No parameters
	call DelayFrame ; don't clobber anything
	call DelayFrame
	call DelayFrame
	ret
	
DelayFrames:
; Delay # frames
; c = number of frames to wait
	call DelayFrame
	dec c
	jr nz,DelayFrames
	ret

GBFadeOutToWhite:
; Fades screen to white
; No parameters
	ld hl, FadePal6
	ld b, 3
.fade
	ld a, [hli]
	ld [rBGP], a
	ld a, [hli]
	ld [rOBP0], a
	ld a, [hli]
	ld [rOBP1], a
	ld c, 4
	call DelayFrames
	dec b
	jr nz, .fade
	ret
	
GBFadeInFromWhite:
; Fades screen from white
; No parameters
	ld hl, FadePal7 + 2
	ld b, 3					; how many stages
.fade
	ld a, [hld]
	ld [rOBP1], a
	ld a, [hld]
	ld [rOBP0], a
	ld a, [hld]
	ld [rBGP], a
	ld c, 8
	call DelayFrames		; make sure it fades
	dec b
	jr nz, .fade
	ret
	
; ---------------------------------------------------------------------------
; General functions (some are from gbhw.inc)
; ---------------------------------------------------------------------------

GetKeypress:
	xor a
	set 5, a				; P15 (buttons)
	ld [rJOYP], a
	nop
	nop
	nop
	nop
	nop						; wait a few cycles
	ld a, [rJOYP]
	cpl						; invert
	and a, $0F				; get A, B, Select, Start
	swap a					; put bits 0-3 to 4-7
	ld b, a
	xor a
	set 4, a				; P14 (direction keys)
	ld [rJOYP], a
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop						; wait a few cycles
	ld a, [rJOYP]
	cpl 					; invert
	and a, $0F				; get up, down, left, right
	or b					; combine
	ld [v_CurrentKP], a		; save to RAM
; Button layout is:
; 8   7   6   5   4   3   2   1   0
; STA SEL B   A   D   U   L   R
; $80 $40 $20 $10 $08 $04 $02 $01
	ret

FillArea:
; Fill a box in BG map with some tile
; a = fill byte
; hl = dest
; bc = height and width
	push bc
	push hl
.a
	push af
	call WaitVRAM
	pop af
	ld [hli], a
	dec c
	jr nz, .a
	pop hl
	pop bc
	dec b
	ret z
	push bc
	ld d, $20
.b
	inc hl
	dec d
	jr nz, .b
	push hl
	jr .a
	
DMACrap:
; transfer DMA code to HRAM
; No parameters
	ld hl, H_DMA
	ld de, .code
	ld bc, .endi - .code
	jp CopyVRAM					; copy all of it
.code:
; this code is copied to HRAM as data
	push	af
	ld	a, a_Sprites/$100		; bank where OAM DATA is stored
	ldh	[rDMA], a			; Start DMA
	ld	a, $28				; 160ns
.wait:
	dec	a
	jr	nz, .wait
	pop	af
	reti
.endi

DisableLCD:
; Disables LCD to allow some VRAM-related operations
; No parameters
	ld a, [rLCDC]
	rlca			; put highest bit on carry flag
	ret nc			; exit if screen is off already
.wait:
	ld a,[rLY]
	cp 144			; V-blank?
	jr c,.wait		; keep waiting if not
	ld a,[rLCDC]
	res 7,a			; reset LCD enabled flag
	ld [rLCDC],a
	ret
	
EnableLCD:
; Enables the GameBoy LCD
; No parameters
	ld a, [rLCDC]
	set 7, a
	ld [rLCDC], a
	ret

CheckLine:
; Waits until a scanline is rendering
; d = scanline # to check
	ld a, [rLY]
	cp d
	jr nz, CheckLine
	ret
	
WaitVRAM:
; Waits until it's okay to write to VRAM
; No parameters
	di
	ld a, [rSTAT]
	bit 1, a
	jr nz, WaitVRAM		; if 1 (still used), wait
	reti
	
FillVRAM:
; Fill VRAM with a value for # bytes
; a  = value
; hl = dest
; bc = bytecount
; d  = backup for a
	push de
	ld d, a
.loop
	call WaitVRAM
	ld a, d
	ld [hli], a
	dec bc
	ld a, b
	or c
	jr nz, .loop
	pop de
	ret
	
	
CopyVRAM:
; Copy a bunch of data to VRAM
; hl = dest
; de = src
; bc = bytecount
	call WaitVRAM
	ld a, [de]
	inc de
	ld [hli], a
	dec bc
	ld a, c
	or b
	jr nz, CopyVRAM
	ret
	
FillRAM:
; Fill normal RAM with a value for # bytes
; a  = value
; hl = dest
; bc = bytecount
; d  = backup for a
	push de
	ld d, a
.loop
	ld a, d
	ld [hli], a
	dec bc
	ld a, b
	or c
	jr nz, .loop
	ld a, d
	pop de
	ret
	
	
CopyRAM:
; Copy a bunch of data to RAM
; hl = dest
; de = src
; bc = bytecount
	ld a, [de]
	inc de
	ld [hli], a
	dec bc
	ld a, c
	or b
	jr nz, CopyRAM
	ret
	
CopyRAM1bpp:
; Copy data twice, e.g. for 1bpp art (such as fonts)
; hl = dest
; de = src
; bc = bytecount
	ld a, [de]
	inc de
	ld [hli], a			; copy twice
	ld [hli], a
	dec bc
	ld a, c
	or b
	jr nz, CopyRAM1bpp
	ret
	
ClearScreen:
; Clears screen with tile $FF
; No parameters
	ld hl, vBGMap0	;9800
	ld bc, $240
	ld a, $FF
	jp FillVRAM
	
ClearSprites:
; Clears the sprite RAM
; No parameters
	ld hl, a_Sprites	; clear oam
	ld bc, $100
	xor a				; ld a, 0
	jp FillRAM
