; ===========================================================================
;				 FOREST RAVE ANIMATION MEME but an actual ROM
;			               (Mishu's Adventure)
;
;	                  GalacticMemes (Zumi / Danni)
;			         Original meme by Nifty-Senpai
;
; 				This is made for compilation with RGBASM
;      source is available at https://gitlab.com/zumid/forest-rave-gb
;
; 			         Please excuse my crappy code
; ===========================================================================

; ===========================================================================
; Constants, macros
; ===========================================================================

INCLUDE "./include/hardware.asm"
INCLUDE	"./include/sound/wram.asm"
INCLUDE	"./include/wram.asm"
INCLUDE	"./include/macros.asm"
INCLUDE	"./include/constants.asm"

; ===========================================================================
; rst vectors
; ===========================================================================


SECTION "rst 00", ROM0 [$00]
	reti
SECTION "rst 08", ROM0 [$08]
	reti
SECTION "rst 10", ROM0 [$10]
	reti
SECTION "rst 18", ROM0 [$18]
	reti
SECTION "rst 20", ROM0 [$20]
	reti
SECTION "rst 28", ROM0 [$28]
	reti
SECTION "rst 30", ROM0 [$30]
	reti
SECTION "rst 38", ROM0 [$38]
	reti

; ===========================================================================
; Hardware interrupts
; ===========================================================================


SECTION "vblank", ROM0 [$40]
	jp Vblank
SECTION "hblank", ROM0 [$48]
	reti
SECTION "timer",  ROM0 [$50]
	reti
SECTION "serial", ROM0 [$58]
	reti
SECTION "joypad", ROM0 [$60]
	reti

; ===========================================================================
; Header
; ===========================================================================

SECTION "Program Entry", ROM0 [$100]

ProgramEntry:
	nop
	jp Start

; Nintendo(TM) logo
	db $CE, $ED, $66, $66, $CC, $0D, $00, $0B, $03, $73, $00, $83, $00, $0C
	db $00, $0D, $00, $08, $11, $1F, $88, $89, $00, $0E, $DC, $CC, $6E, $E6
	db $DD, $DD, $D9, $99, $BB, $BB, $67, $63, $6E, $0E, $EC, $CC, $DD, $DC
	db $99, $9F, $BB, $B9, $33, $3E

	db "FOREST RAVE    "	; Game title
	db $00			; GameBoy type
	db "Zu"			; New license
	db $00			; SGB flag
	db $01			; Cart type
	db $00			; ROM size, handled by RGBFIX
	db $00			; RAM size
	db $01			; Destination code
	db $33			; Old license
	db $01			; ROM version
	db $00			; Complement checksum, handled by RGBFIX
	dw $0000		; Global checksum, handled by RGBFIX

; ===========================================================================
; Begin
; ===========================================================================	

SECTION "Main Program", ROM0 [$150]

INCLUDE	"./include/functions.asm"
	
; ===========================================================================
; PROGRAM START
; ===========================================================================

Start:
	di					; init the whole thing
	ld sp, $DFFF		; set stack
	ld a, %11100100		; set background palette
	ld [rBGP],a
	
	xor a
	ld [rSCX],a			; reset scroll
	ld [rSCY],a
	
; begin clearing GB ram
	ld hl, $c000
	ld bc, $dfff - $c000
.clearwram
	xor a
	ld [hli], a
	dec bc
	ld a, c
	or b
	jr nz, .clearwram
	
	call DMACrap		; because we need sprites duh
	
; blank out viewable area..

;	jp BeginMovie		; skip disclaimer
	call ClearScreen

Disclaimer:
	ld hl, vBGMap0 + $20
	ld de, Beginning_Text
	call ParseText
	
	loadVRAM vChars0 + $200, GFX2, GFX2_End - GFX2 ; ASCII set
	
	ld a, %10010001		
	ld [rLCDC], a
	
	ld a,%00000001  	; Enable V-blank interrupt
	ld [rIE], a
	ei
	
.getkeys
	call GetKeypress
	ld a, [v_CurrentKP]
	cp 8				; Start pressed?
	jr nz, .getkeys
	
	call GBFadeOutToWhite	
	call DisableLCD

; ===========================================================================
; MOVIE INIT
; ===========================================================================

BeginMovie:	
	di
	call DisableLCD
	call ClearScreen
	loadVRAM vChars0, GFX1, GFX1_End - GFX1 ; -> to 8000
	
; fill with bg tiles
	ld hl, vBGMap0
	ld de, Title_Map
	call ParseMap
; line between "MISHU's" and "ADVENTURE"
	ld hl, $9884
	ld bc, $9890-$9884
	ld a, $1C
	call FillRAM
; drop shadow
	ld hl, $98E0
	ld bc, $98E0-$98C0
	ld a, $26
	call FillRAM
; grass
	ld hl, $99C0
	ld bc, $20
	ld a, $2A
	call FillRAM
	ld hl, $99E0
	ld bc, $9A40 - $99E0
	ld a, $2D
	call FillRAM	
; putting textures
; in hardcoded spots...
	ld a, $2C
	ld [$9A00], a
	ld [$9A06], a
	ld [$9A0D], a
	ld [$9A14], a
	ld [$9A1C], a
	ld a, $2B
	ld [$9A20], a
	ld [$99EA], a
	ld [$99F7], a

	ld a, %10010011		; turn the screen on, with:
						; bg map at 9800
						; tiles at 8000
						; and sprites
	ld [rLCDC], a
	
	xor a				; hit it...
	call Music_Play
	
	ld a,%00000001  	; Enable V-blank interrupt
	ld [rIE], a
	ei
	
; We have liftoff

; ---------------------------------------------------------------------------
; Scene functions
; ---------------------------------------------------------------------------

LoadScenes:
	xor a
	ld b, a
	ld a, [v_CurScene]
	ld c, a					; bc = scene # (8-bit)
	
	ld hl, SceneList		; fetch list
	add hl, bc
	add hl, bc				; get pointer
	
	push hl	
	pop de					; hl -> de
	ld a, [de]
	inc de
	
	ld l, a
	ld a, [de]
	ld h, a					; grab location to hl
	jp hl					; and jump

JumpToNewScene:
	ld hl, v_CurScene
	inc [hl]				; increase the current scene number
	jr LoadScenes
	
CheckIfSceneIsDone:
; sets carry flag if scene is done
	call DelayFrame
	xor a
	ld b, a
	ld a, [v_CurScene]
	inc a					; get next scene number
	ld c, a					; bc = current scene + 1
	ld hl, SceneCues
	add hl, bc				; fetch cue list and grab
							; that scene's bar number
	ld a, [hl]
	ld b, a
	ld a, [w_song_position]
	cp a, b					; check song bar for cue number
	ccf
	ret nz					; don't carry
	scf
	ret						; set carry if we're good to go

; ---------------------------------------------------------------------------
; Scene routines
; ---------------------------------------------------------------------------

INCLUDE	"./scenes/intro.asm"
INCLUDE	"./scenes/playerselect.asm"
INCLUDE	"./scenes/loading.asm"
INCLUDE	"./scenes/stage.asm"
INCLUDE	"./scenes/boss.asm"
INCLUDE	"./scenes/end.asm"

; ---------------------------------------------------------------------------
; V-Blank
; ---------------------------------------------------------------------------

Vblank:
	push af
	push bc
	push de
	push hl
	xor a
	ld [H_VBF], a
	call H_DMA					; run HRAM-resident OAM code
	call UpdateMusic			; work that bloated band
	pop hl
	pop de
	pop bc
	pop af
	reti

; ---------------------------------------------------------------------------
; Sound Engine & Deeta
; ---------------------------------------------------------------------------

INCLUDE "./include/sound/constants.asm"
INCLUDE "./include/sound/macros.asm"
INCLUDE "./sound/engine.asm"
INCLUDE "./sound/interfaces.asm"
INCLUDE "./sound/sounds.asm"

INCLUDE "./include/functions-fr.asm"

; ===========================================================================
; GFX and Maps
; ===========================================================================

SECTION "Data",ROMX,BANK[$1]

; ---------------------------------------------------------------------------
; Tilesets
; ---------------------------------------------------------------------------

GFX1:	; Title screen
INCBIN "./gfx/title.2bpp"
GFX1_End:

GFX2:	; ASCII character set (capitals, punc, numbers only)
INCBIN "./gfx/ascii-set.2bpp"
GFX2_End:

GFX3:	; "PLAYERSCT"
INCBIN "./gfx/playersct.2bpp"
GFX3_End:

GFX4:	; Player select: Mishu art
INCBIN "./gfx/PS-mishu.2bpp"
GFX4_End:

GFX5:	; Battle GUI
INCBIN "./gfx/battle-tiles.2bpp"
GFX5_End:

GFX6:	; Lumi's battle sprite [front]
INCBIN "./gfx/BA-boss.2bpp"
GFX6_End:

GFX7:	; Player battle sprite [back]
INCBIN "./gfx/BA-player.2bpp"
GFX7_End:

GFX8:	; Player select: DD art
INCBIN "./gfx/PS-dd.2bpp"
GFX8_End:

GFX9:	; Stage art
INCBIN "./gfx/stage.2bpp"
GFX9_End:

GFX10:	; Training... art
INCBIN "./gfx/sona-sleep.2bpp"
GFX10_End:

GFX11:	; starburst
INCBIN "./gfx/gfx-hit.2bpp"
GFX11_End:

GFX12:	; water
INCBIN "./gfx/gfx-wataaaa.2bpp"
GFX12_End:

GFX13:	; Mishu frames [stage]
INCBIN "./gfx/stage-Frames.2bpp"
GFX13_End:

; ---------------------------------------------------------------------------
; Tilemaps
; ---------------------------------------------------------------------------

; Format of tilemaps:
; [X] [Y] [data length]
; [data]
; [X] [Y] [data length]
; [data]
; ...
; $80 (Mark data end)

Title_Map:
	db 4, 1, 12	; Absolute position in relation to top left tile
				; and length of data
	db 0,1,2,3,4,5,6,7,8,9,3,4
	db 4, 2, 12
	db 10,11,12,13,14,15,16,17,18,$2F,$0D,$0E
	db 4, 3, 12
	db $13,$14,$15,$16,$17,$18,$19,$1A,$1B,$2F,$16,$17
	db 7, 5, 5
	db $1D,$1E,$1F,$20,$21
; clouds.
	db 3, 8, 3
	db $27, $28, $29
	db 15, 9, 3
	db $27, $28, $29
	db $80		; stop processing
	
ForestRave_Map:
	db 4, 5, 11
	db $22, $23, $24, $1F, $20, $21
	db $7C
	db $24, $25, $2E, $1F
	db $80
	
Sleepy_Map:
	db 3+1, 6, 3
	db $60,$61,$62
	db 3+1, 7, 9
	db $63,$64,$65,$66,$67,$68,$69,$6a,$6b
	db 2+1, 8, 10
	db $6c,$6d,$6e,$6f,$70,$71,$72,$73,$74,$75
	db 2+1, 9, 10
	db $76,$77,$78,$79,$7a,$7b,$7c,$7d,$7e,$7f
	db 2+1, 10, 11
	db $80,$81,$82,$83,$84,$85,$86,$87,$88,$89,$8a
	db 0+1, 11, 12
	db $8b,$8c,$8d,$8e,$8f,$90,$91,$92,$93,$94,$95,$96
	db 0+1, 12, 12
	db $97,$98,$99,$9a,$9b,$9c,$9d,$9e,$9f,$a0,$a1,$a2
	db 0+1, 13, 13
	db $a3,$a4,$a5,$a6,$a7,$a8,$a9,$aa,$ab,$ac,$ad,$ae,$af
	db 0+1, 14, 13
	db $b0,$b1,$b2,$b3,$b4,$b5,$b6,$b7,$b8,$b9,$ba,$bb,$bc
	db 0+1, 15, 13
	db $bd,$be,$bf,$c0,$c1,$c2,$c3,$c4,$c5,$c6,$c7,$c8,$c9
	db 0+1, 16, 14
	db $ca,$cb,$cc,$cd,$ce,$cf,$d0,$d1,$d2,$d3,$d4,$d5,$d6,$d7
	db 0+1, 17, 14
	db $d8,$d9,$da,$db,$dc,$dd,$de,$df,$e0,$e1,$e2,$e3,$e4,$e5
	db $80
	
PlayerSelect_Map:
	db 4, 1, 13
	db $5B, $5C, $5D, $5E, $5F, $60, $20, $61, $5F, $5C, $5F, $62, $63
	db 4, 2, 13
	db $5B+9, $5C+9, $5D+9, $5E+9, $5F+9, $60+9, $20, $61+9, $5F+9, $5C+9, $5F+9, $62+9, $63+9
	db 4, 3, 13
	db $5B+18, $5C+18, $5D+18, $5E+18, $5F+18, $60+18, $20, $61+18, $5F+18, $5C+18, $5F+18, $62+18, $63+18
	db $80
	
Loading_Map:
	db 6, 3, .b - .a
.a
	db "LOADING..."
.b
	db $80
	
HP_Map: ; Health bars, etc.
	db 4-3, 1, .b - .a			; lv. 31
.a
	db "LUMI ", $1b, "31"
.b								; HP first row
	db 1,2,.h-.g
.g
	db 7,8,9,10,10,10,10,10,10,12
.h								; HP second row
	db 1,3,.j-.i
.i
	db $e,$f,$f,$f,$f,$f,$f,$f,$f,$10
.j
	db 14-5, 9, .l - .k			; lv. 16
.k
	db "MISHU ", $1b, "16"
.l								; HP first row
	db 9,10,.n-.m
.m
	db 7,8,9,10,10,10,10,10,10,12
.n								; HP second row
	db 9,11,.p-.o
.o
	db $e,$f,$f,$f,$f,$f,$f,$f,$f,$10
.p
	db $80
BattleBG_Map:	; BG elements
	db 11,1,.f-.e			
.e
	db 4,0,0,0,0,0,0,6
.f
	db $80

; Text-map format:
; [text]
; # (new line character)
; [text]
; ...
; $5B (end character)
	
Text1_Map:
	db "  MISHU  # #"
	db "* EDGE   #"
	db "  LORD   # #"
	db "* WILL   #"
	db "  EAT    #"
	db "  YOU    #"
	db "         #"
	db "* THICC  #"
	db "         "
	db $5B
	
Text2_Map:
	db "  D.D.   # #"
	db "* JUST   #"
	db "  EDGY   # #"
	db "* BLUD   #"
	db "  XDDD   #"
	db "         #"
	db "         #"
	db "* SILENT #"
	db "         "
	db $5B
	
Text3_Map:
	db "LOADING..."
	
End_Map:
	db "    THANKS  FOR# #"
	db "     WATCHING!# # #"
	db "    ORIGINAL BY# #"
	db "    NIFTY-SENPAI# # #"
	db " SOURCE CODE ON# #"
	db "   ZUMID @ GITLAB# # #"
	db "        2018"
	db $5B
	
Beginning_Text:
	db "  PLEASE NOTE THAT# #"
	db "    THIS IS NOT# #"
	db "  A PLAYABLE GAME.# #"
	db "  THIS IS A MOVIE# #"
	db "   DEMO MADE AS A# #"
	db "    PROGRAMMING# #"
	db "     PRACTICE.# # #"
	db "   -PRESS START-"
	db $5B
	
Stage_Intro_Text:
	db "CHALLENGE",$7F
	db "     ACCEPTED!!"
	db $FF
	
Sleepy_Text:
	db "WHY IS THIS# #",
	db " GUY EVEN HERE?"
	db $5B
	
Stage_1_Text:
	db "LEVEL 1!!!"
	db $5B
	
Stage_2_Text:
	db "LEVEL 2!!!"
	db $5B
	
Stage_2A_Text:
	db "INCOMING BOSS!!"
	db $5B

; Dialog format
; [text]
; $7F (line break)
; [text]
; ...
; $FF (end)

Dialog_Battle1:
	db "LUMI WANTS"
	db $7F					; line break character
	db "TO FIGHT!"
	db $FF					; end character
	
Dialog_Battle2:
	db "MISHU USED"
	db $7F					; line break character
	db "CUT!"
	db $FF					; end character
	
Dialog_Battle4:
	db "BUT IT FAILED!"
	db $FF					; end character
	
Dialog_Battle5:
	db "LUMI USED"
	db $7F					; line break character
	db "KICK!"
	db $FF					; end character
	
Dialog_Battle6:
	db "MISHU"
	db $7F					; line break character
	db "FAINTED..."
	db $FF					; end character
	
Dialog_Battle7:
	db "MISHU USED"
	db $7F					; line break character
	db "CUT!"
	db $FF					; end character
	
Dialog_Battle8:
	db "LUMI USED"
	db $7F					; line break character
	db "WATER GUN!"
	db $FF					; end character
	
Dialog_Battle9:
	db "MISHU USED"
	db $7F					; line break character
	db "???!"
	db $FF					; end character
	
Dialog_Battle10:
	db "IT'S SUPER"
	db $7F					; line break character
	db "EFFECTIVE!"
	db $FF					; end character
	
Dialog_Battle11:
	db "LUMI"
	db $7F					; line break character
	db "FAINTED..."
	db $FF					; end character
	
Dialog_Battle12:
	db "MISHU GAINED"
	db $7F					; line break character
	db "343 EXP.!"
	db $FF					; end character
	
Text_BattleSelection:
	db "FIGHT PTY#"
	db " #"
	db "BAG   RUN"
	db $5B
	
Text_MoveSelection:
	db "CUT#CHOKEHOLD#??? ???#PUNCH"
	db $5B

; dialog limits
; XXXXXXXXXXXXXXXXXX
; XXXXXXXXXXXXXXXXXX

; ---------------------------------------------------------------------------
; Misc. Data
; ---------------------------------------------------------------------------

CS_CharaTable:
	dw GFX4, Text1_Map	; Mishu
	dw GFX8, Text2_Map	; DD
	
Sine_Data:
	db 0,1,3,4,5,6,7,7,7,7,7,6,5,4,3,1,0,-2,-4,-5,-6,-7,-8,-8,-8,-8,-8,-7,-6,-5,-4,-2
		
GetSine:
; a = entry number
; clobbers hl and bc
	ld c, a
	xor a
	ld b, a
	ld hl, Sine_Data
	add hl, bc
	ld a, [hl]
	ret

FadePal1: db %11111111, %11111111, %11111111
FadePal2: db %11111110, %11111110, %11111000
FadePal3: db %11111001, %11100100, %11100100
FadePal4: db %11100100, %11010000, %11100000
;                rBGP      rOBP0      rOBP1
FadePal5: db %11100100, %11100100, %11100100
FadePal6: db %10010000, %10000000, %10010000
FadePal7: db %01000000, %01000000, %01000000
FadePal8: db %00000000, %00000000, %00000000

SceneList:
	dw Scene_Intro
	dw Scene_PlayerSelect
	dw Scene_Loading
	dw Scene_Stage
	dw Scene_Boss
	dw Scene_End
	
SceneCues:
	db	$00	; Intro
	db  $08	; Level Select
	db  $0C	; Loading...
	db  $0D	; Stages
	db	$1C-1	; Battle
	db	$2A	; End
