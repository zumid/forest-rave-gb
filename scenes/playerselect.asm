; ===========================================================================
; "Player Select"
; ===========================================================================

Scene_PlayerSelect:
	xor a
	ld [rSCX],a
	
	call DisableLCD
	call ClearScreen
	call ClearSprites
	
	ld hl, vChars0 + $760		; clear tile space
	ld bc, $E40 - $760			; from $76 - $E3
	call FillRAM
	
	loadVRAM vChars0 + $200, GFX2, GFX2_End - GFX2 ; ASCII set
	loadVRAM vChars0 + $5B0, GFX3, GFX3_End - GFX3 ; PLAYERSCT
	
	ld hl, vBGMap0
	ld de, PlayerSelect_Map
	call ParseMap				; "PLAYER SELECT" text
	
	ld hl, $9800 + (6*$20) + 1	; where to put the grand
								; motherload of tiles
								
	ld a,  $76					; starting tile
	ld de, $0B0A				; rows ($0B) and columns ($0A)
	call GenerateMap
	
	call EnableLCD
	call GBFadeInFromWhite

; ===========================================================================

.loop
; Process text
	ld a, [v_PS_CharaCount]
	ld hl, CS_CharaTable
	ld b, 0
	ld c, a							; load bc with current character
	add hl, bc
	add hl, bc
	add hl, bc
	add hl, bc						; get data location
	inc hl
	inc hl							; get text pointer
	ld a, [hli]
	ld e, a
	ld a, [hl]
	ld d, a							; pointer to de
	ld hl, $9800 + (6*$20) + 11		; where to put text
	call ParseText
; Process art
	ld a, [v_PS_CharaCount]
	ld hl, CS_CharaTable
	ld b, 0
	ld c, a
	add hl, bc
	add hl, bc
	add hl, bc
	add hl, bc						; ditto
	ld a, [hli]
	ld e, a
	ld a, [hl]
	ld d, a							; art location -> de
	ld hl, vChars0 + $760
	ld bc, GFX4_End - GFX4			; Reference size
	call CopyVRAM
	
	ld c, 45
	call DelayFrames				; hold for about 1.333s (60 / 45)
	
	ld a, [v_PS_CharaCount]
	inc a
	cp PS_CHARAMAX
	jr nz, .assignchara
	xor a							; reset character variable
									; if exceeding max. chara amount
.assignchara
	ld [v_PS_CharaCount], a
	
; ===========================================================================

	call CheckIfSceneIsDone
	jp c, JumpToNewScene 
	jp .loop
