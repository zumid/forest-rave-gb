; ===========================================================================
; Stage 1 / 2
; ===========================================================================
Scene_Stage:
	call ClearScreen
	call DisableLCD
	loadVRAM vChars0 + $5b0, GFX13, GFX13_End - GFX13
	loadVRAM vChars0, GFX9, GFX9_End - GFX9
; Set the stage...
	fill vBGMap0, $00, $20
	fill vBGMap0+$20, $01, $20
	
;-- ground, top layer --
	ld a, 8
	ld c,	16
	ld hl,	$9980
.grounds0
	ld [hli], a
	inc a
	ld [hli], a
	dec a
	dec c
	jr nz, .grounds0
	
;-- ground, bottom layers --
	ld a, 10
	ld c,	16*5
	ld hl,	$9980+$20
.grounds1
	ld [hli], a
	inc a
	ld [hli], a
	dec a
	dec c
	jr nz, .grounds1
	
;-- pyramids upper --
	ld a, $20
	ld c,	3				; # of times to put in some blocks
	ld hl,	$9900
.pyramids0
	ld [hli], a
	ld a, 2
	ld [hli], a
	ld a, 3
	ld [hli], a
	ld a, $20
	ld [hli], a
	ld [hli], a
	ld [hli], a
	ld [hli], a
	dec c
	jr nz, .pyramids0
	
;-- pyramids lower --
	ld c,	3				; # of times to put in some blocks
	ld hl,	$9900+$20
.pyramids1
	ld a, 2
	ld [hli], a
	ld a, $20
	ld [hli], a
	ld [hli], a
	ld a, 3
	ld [hli], a
	ld a, 2
	ld [hli], a
	inc a
	ld [hli], a
	ld a, $20
	ld [hli], a
	dec c
	jr nz, .pyramids1
	
;-- ocean top --
	ld a, 4
	ld c,	16
	ld hl,	$9940
.ocean0
	ld [hli], a
	inc a
	ld [hli], a
	dec a
	dec c
	jr nz, .ocean0
	
;-- ocean bottom --
	ld a, 6
	ld c,	16
	ld hl,	$9940+$20
.ocean1
	ld [hli], a
	inc a
	ld [hli], a
	dec a
	dec c
	jr nz, .ocean1
	call EnableLCD
	
; Make sprites
	ld a, $BF
	ld hl, a_Sprites
	ld bc, $0504
	ld d, 64
	ld e, 56
	call GenerateOAM
	
; Dialoge
	ld hl, vBGMap0 + 2 + (3*$20)
	ld de, Stage_Intro_Text
	call TypeText
	
; Initialize scroll vars
	xor a
	ld hl, v_Scrollx
	ld [hli], a
	ld [hl], a
	
; Wait for song sync
.wait0
	ld a, [w_song_position]
	cp $0E
	jr nz, .wait0
; Replace text with "Level 1"
	ld a, $FF
	ld hl, vBGMap0 + 2 + (3*$20)
	ld bc, $030F
	call FillArea
	ld hl, vBGMap0 + 5 + (2*$20)
	ld de, Stage_1_Text
	call ParseText
.wait1
	ld a, [w_song_position]
	cp $0F
	jr nz, .wait1
; Clear text
	ld a, $FF
	ld hl, vBGMap0 + 5 + (2*$20)
	ld bc, $010A
	call FillArea

; ========== BEGIN MOVING PLAYER =============

	xor a
	ld [v_SpriteTimer], a
	ld a, $5B
	ld [v_CurSprite], a
Part1:
; === Move BG === 
	ld c, 40
.a
	call Run_Player_Sprites
	call MoveBG
	call DelayFrame
	dec c
	jr nz, .a
	

Part2:
; === Make Rock 1 === 
	ld hl, a_Sprites + $50
	ld a, 88 + 8 + 8
	ld [hli],a
	inc hl
	ld a, t_Rock					; rock tile
	ld [hl],a
; === Run towards rock 1 === 
	ld c, 50
.a
	call Run_Player_Sprites
	call MoveBG
	call MoveRock1
	call DelayFrame					; run for 50 frames
	dec c
	jr nz, .a
;  === Jump rock 1 === 
	ld c, 40
.b
	call JumpPlayer
	ld a, $AB
	call Run_Player_Sprites_Assign
	call MoveBG
	call MoveRock1
	call DelayFrame
	dec c
	jr nz, .b
	
	
Part3:
; === Make Rock 2 === 
	ld hl, a_Sprites + $50 + 4
	ld a, 88 + 8 + 8
	ld [hli],a
	inc hl
	ld a, t_Rock					; rock tile
	ld [hl],a
;  === Run towards rock 2 === 
	ld c, 50
.a
	call JumpPlayer
	call Run_Player_Sprites
	call MoveBG
	call MoveRock1
	call MoveRock2
	call DelayFrame
	dec c
	jr nz, .a
;  === Jump rock 2 === 
	ld c, 50
	xor a
	ld [v_MovementIndex], a		; reset jump position
.b
	call JumpPlayer
	ld a, $AB
	call Run_Player_Sprites_Assign
	call MoveBG
	call MoveRock1
	call MoveRock2
	call DelayFrame
	dec c
	jr nz, .b


Part4:
;  === Reuse rock 1 -> rock 3 === 
	ld hl, a_Sprites + $50 + 1
	xor a
	ld [hl],a
;  === Run towards 3 === 
	ld c, 50
.a
	call JumpPlayer
	call Run_Player_Sprites
	call MoveBG
	call MoveRock1
	call MoveRock2
	call DelayFrame
	dec c
	jr nz, .a
;  === Jump 3 === 
	ld c, 12
	xor a
	ld [v_MovementIndex], a		; reset jump position
.b
	call JumpPlayer
	ld a, $AB
	call Run_Player_Sprites_Assign
	call MoveBG
	call MoveRock1
	call MoveRock2
	call DelayFrame
	dec c
	jr nz, .b
	
	
Part5:
;  === Reuse rock 2 -> rock 4 === 
	ld hl, a_Sprites + $50 + 4 + 1
	xor a
	ld [hl],a
;  === Run 4 === 
	ld c, 55
.a
	call JumpPlayer
	call Run_Player_Sprites
	call MoveBG
	call MoveRock1
	call MoveRock2
	call DelayFrame
	dec c
	jr nz, .a
;  === Jump 4 === 
	xor a
	ld [v_MovementIndex], a		; reset jump position
	ld c, 50
.b
	call JumpPlayer
	ld a, $AB
	call Run_Player_Sprites_Assign
	call MoveBG
	call MoveRock1
	call MoveRock2
	call DelayFrame
	dec c
	jr nz, .b
.c
	call Run_Player_Sprites
	call MoveBG
	call MoveRock1
	call MoveRock2
	call DelayFrame
	ld a, [w_song_position]
	cp $14
	jr nz, .c
	
; "Level 2" which is basically nothing

Part6:
	ld hl, vBGMap0 + 5 + (2*$20)
	ld de, Stage_2_Text
	call ParseText
.a
	call MoveRock2
	call Run_Player_Sprites
	call MoveBG
	call DelayFrame
	ld a, [w_song_position]
	cp $15
	jr nz, .a
	
	ld a, $FF
	ld hl, vBGMap0 + 5 + (2*$20)
	ld bc, $010A
	call FillArea
	
	ld a, 1
	ld [v_SpeedFlag], a
	
Part7:
	ld a, 8						; to movement data
	ld [v_MovementIndex], a
	ld [v_MovDirection], a		; X axis
.a
	call JumpPlayer
	call Run_Player_Sprites
	call MoveBG
	call DelayFrame
	ld a, [w_song_position]
	cp $18
	jr nz, .a
	
Part8:							; DD flies in
	ld a, $D3
	ld hl, a_Sprites+$50
	ld bc, $0504
	ld d, 38
	ld e, 184
	call GenerateOAM
	ld c, 90
.a
	call MoveDoubledot
	call JumpPlayer
	call Run_Player_Sprites
	call MoveBG
	dec c
	jr nz, .a
	
	ld hl, vBGMap0 + 2 + (2*$20)
	ld de, Stage_2A_Text			; "INCOMING BOSS!!"
	call ParseText
	
.loop
	call MoveDoubledot
	call JumpPlayer
	call Run_Player_Sprites
	call MoveBG
	
; ===========================================================================

	call CheckIfSceneIsDone
	jp c, JumpToNewScene 
	jr .loop
	
; ===========================================================================
; Scene Functions
; ===========================================================================

MoveRock1:
	ld hl, a_Sprites + $50 + 1	; move rock
	inc [hl]
	ret
MoveRock2:
	ld hl, a_Sprites + $50 + 4 + 1	; move rock
	inc [hl]
	ret
	
MoveBG:
	ld hl, v_ScrollTimer
	ld a, [hl]
	cp 11
	jr z, .x
	inc [hl]
	jr .y
.x
	xor a
	ld [hl], a
	ld hl, v_Scrollx2
	dec [hl]
.y
	ld hl, v_Scrollx
	dec [hl]
	ld a, [v_SpeedFlag]
	and a
	jr z, .z
	dec [hl]
.z
	ld hl, rSCX
	waitY 64
	ld a, [v_Scrollx2]
	ld [hl], a
	waitY 96
	ld a, [v_Scrollx]
	ld [hl], a
	waitY 143
	xor a
	ld [hl], a
	ret
	
JumpPlayer:
	ld a, [v_MovCounter]
	and a
	jr z, .ppp					; if no counter
	dec a
	ld [v_MovCounter], a
	ld a, [v_MovValue]
	ld b, a
	jr .update					; move player
; get new
.ppp
	ld hl, PlayerJumpData
	ld a, [v_MovementIndex]
	ld e, a
	xor a
	ld d, a
	add hl, de
	add hl, de					; get index #
	ld a, [hli]
	cp $50
	ret z						; refrain if end of data
	ld [v_MovValue], a
	ld b, a						; get delta
	ld a, [hl]
	ld [v_MovCounter], a		; get repeat count
	ld hl, v_MovementIndex
	inc [hl]					; increase movement index
.update
	ld d, 5*4					; width and height
	ld hl, a_Sprites
	ld a, [v_MovDirection]
	and a
	jr z, .update2				; if zero, update Y
	inc hl						; otherwise update X instead
.update2
	ld a, [hl]
	add a, b
	ld [hli], a
	inc hl
	inc hl
	inc hl
	dec d
	jr nz, .update2
	ret
PlayerJumpData:
; Jump data
	db -5, 2
	db -2, 7
	db -1, 3
	db 0, 14
	db 1, 3
	db 2, 7
	db 5, 2
	db $50, $50				; end
; Movement data
	db 0, 90
	db 1, 46-20
	db 0, 50+20
	db -1, 55-20
	db 0, 74+20
	db -1, 64-20
	db 1, 48+20
	db 0, 23
	db -2, 74
	db $50, $50
	
Run_Player_Sprites:
	ld a, [v_SpriteTimer]
	and a
	jr z, .newsprite
	dec a
	ld [v_SpriteTimer], a
	ret
.newsprite
	ld a, 6
	ld [v_SpriteTimer], a
	ld a, [v_CurSprite]
	ld b, 4*5
	add a, b
	cp $5B + 4*(4*5)
	jr nz, .notreset
	ld a, $5B
.notreset
	ld [v_CurSprite], a
Run_Player_Sprites_Assign:
	push bc
; where A is the origin
	ld bc, $0504
	ld hl, a_Sprites
.as0
	inc hl
	inc hl
	ld [hli], a
	inc hl
	inc a
	dec c
	jr nz, .as0
	ld c, $04
	dec b
	jr nz, .as0
	pop bc
	ret
; base is $5B

MoveDoubledot:
	push bc
; where A is the origin
	ld bc, $0504
	ld hl, a_Sprites + $50
.as0
	inc hl
	dec [hl]
	inc hl
	inc hl
	inc hl
	dec c
	jr nz, .as0
	ld c, $04
	dec b
	jr nz, .as0
	pop bc
	ret
	
