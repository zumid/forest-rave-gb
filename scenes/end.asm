; ===========================================================================
; Ending Screen
; ===========================================================================

Scene_End:
	call GBFadeOutToWhite
	call ClearScreen
	call ClearSprites
	ld hl, vBGMap0 + $20
	ld de, End_Map
	call ParseText
	call GBFadeInFromWhite
.loop
	halt
	call GetKeypress
	ld a, [v_CurrentKP]
	cp 8
	jp z, Start
	jr .loop				; Wait patiently 'till a division by zero
							; is somehow possible
