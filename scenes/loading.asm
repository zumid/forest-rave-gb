; ===========================================================================
; "Loading..."
; ===========================================================================
Scene_Loading:
	call DisableLCD
	call ClearScreen
	ld hl, vBGMap0
	ld de, Loading_Map
	call ParseMap
	call EnableLCD
.loop
	halt	
	call CheckIfSceneIsDone
	jp c, JumpToNewScene 
	jr .loop