; ===========================================================================
; Intro Scene
; ===========================================================================

Scene_Intro:
	ld a, 5
	ld [rTAC], a							; Activate the random number 
											; generator

	ld hl, a_Sprites
	ld de, $5040
	ld bc, (SPRITESIZE_Y1 * $100) + SPRITESIZE_X1
	ld a, $7c
	call GenerateOAM
	
	ld a, $30
	ld [v_CurSprite],a
	call AnimateMishu						; Assign Mishu frame tiles
	
	call GBFadeInFromWhite					; transition...
	
; ===========================================================================

.loop
; Process sprites
	ld hl, v_SpriteTimer
	ld a, [hl]
	cp 8
	jr z, .assignnew						; assign new tiles
	inc [hl]
	jr .process_scroll						; skip processing sprites
.assignnew
	xor a
	ld [hl], a
	ld hl, v_CurSprite
	inc [hl]								; v_CurSprite + 4
	inc [hl]
	inc [hl]
	inc [hl]
	ld a, [hl]
	cp $3C
	jr nz, .change
	ld a, $30								; if > $3C then reset origin to $30
.change
	ld [hl], a
	call AnimateMishu						; load mishu sprites

.process_scroll
	; whooh, what a mess
	; there's gotta be a better way
	; to compare variables.
.scroll1									; Ground scrolling
	ld hl, v_ScrollTimer
	ld a, [hl]
	dec a						; cp 2
	dec a
	jr z, .scroll1_
	inc [hl]
	jr .scroll2					; skip
.scroll1_
	ld [hl], a
	ld a, [v_Scrollx]
	dec a
	ld [v_Scrollx], a
.scroll2									; Cloud scrolling
	inc hl
	ld a, [hl]					; v_ScrollTimer2
	cp 24
	jr z, .scroll2_
	inc [hl]
	jr .processBG				; jump to next function
.scroll2_
	xor a
	ld [hl], a					; reset scroll timer
	ld a, [v_Scrollx2]
	dec a
	ld [v_Scrollx2], a
	
.processBG						; Do BG scrolls
	waitY 8
	xor a
	ld [rSCX],a					; Top part
	waitY 64
	ld a, [v_Scrollx2]
	ld [rSCX],a					; Clouds
	waitY $6E
	ld a, [v_Scrollx]
	ld [rSCX],a					; Ground
	
; Check for glitch cue

	ld hl, w_song_position		; get bar
	ld a, [hl]
	ld c, GLITCH_TICK_A			; initial glitch fx compare
	cp c
	jr c, .noeffect				; if it's not the time, don't do anything
	inc c
	inc c						; stops at 2 + glitch_tick_a
	cp c						; compare again
	jr nc, .noeffect
	inc hl						; get ticks
	ld a, [hl]
	dec hl						; bring back for comparing positions later on
	dec a						; ...is 1?
	jr nz, .noeffect
	
; replace "QUEST" with "FOREST RAVE"
	ld hl, vBGMap0
	ld de, ForestRave_Map
	call ParseMap
	
; do some fancy glitch effect
; go rando!
	ld a, [rDIV]
	set 3, a
	cpl
	ld c, a
; wandering around
	push bc
	pop bc
	push hl
	pop hl
; slacking off...
	nop
	nop
	nop
	nop
	nop
	ld a, [rTIMA]
	xor c
	adc a, a
	;ld [rBGP], a		; seizures?
	jr .scy
.noeffect
	;ld a, %11100100
	;ld [rBGP], a
	xor a
.scy
	ld [rSCY], a
	
; ===========================================================================

	call CheckIfSceneIsDone
	jp c, JumpToNewScene 
	jp .loop

; ===========================================================================
; Scene Functions
; ===========================================================================

AnimateMishu:
; sprites used:
; Mishu , frame 0:
; 30 - 33. 40 - 43. ... . 90 - 93.
; Mishu , frame 1:
; 34 - 37. 44 - 47. ... . 94 - 97.
; Mishu , frame 2:
; 38 - 3B. 48 - 4B. ... . 98 - 9B.

; a = starting tile...
	ld hl, a_Sprites
	ld bc, (SPRITESIZE_Y1 * $100) + SPRITESIZE_X1
.column
	inc hl
	inc hl
	ld [hl], a
	inc a
	inc hl
	inc hl
	dec c
	jr nz, .column
	ld c, 4
	sub a, 4
	add a, $10
	dec b
	jr nz, .column
	ret
