; ===========================================================================
; Boss
; ===========================================================================
	
Scene_Boss:
.transition
	ld c, 18		; screen height
	ld b, 9			; number of columns
	ld hl, $9809
.t0
	push hl
	call WaitVRAM
	ld a, $FF
	ld [hl], a
	ld a, [v_Transition_Stage]
	add a
	inc a
	ld d, a
	ld a, l
	add a, d
	ld l, a
	call WaitVRAM
	ld a, $FF
	ld [hl], a
	ld de, $20
	pop hl
	add hl, de
	dec c
	jr nz, .t0
	call DelayFrame		; avoid clobbering c
	call DelayFrame
	call DelayFrame
	call DelayFrame
	call DelayFrame
	call DelayFrame
	ld c, 18
	ld hl, v_Transition_Stage
	inc [hl]
	ld hl, $9809
	ld d, l
	ld a, [v_Transition_Stage]
	sub a, d
	cpl
	inc a
	ld l, a
	dec b
	jr nz, .t0

.init
	call DisableLCD
	call ClearScreen
	call ClearSprites
	loadVRAM vChars0, GFX5, GFX5_End - GFX5			; GUI
	loadVRAM vChars0 + $760, GFX6, GFX6_End - GFX6	; Lumi
	loadVRAM vChars0 + $A00, GFX7, GFX7_End - GFX7	; Mishu
	loadVRAM vChars0 + $5B0, GFX12, GFX12_End - GFX12	; Battle tiles
	loadVRAM vChars0 + $5D0, GFX11, GFX11_End - GFX11	; Battle tiles
	
; Init HP
	ld a, 12					; Max. HP
								; after all this is not a game ;3
	ld [v_HP_Enemy], a
	ld [v_HP_Player], a

.continue
; Filth
	ld hl, vBGMap0
	ld bc, $20
	ld a, 1
	call FillVRAM				; Solid light grey tile
	ld hl, vBGMap0+$20
	ld bc, $20
	ld a, 2						; Striped tile
	call FillVRAM
	ld hl, vBGMap0
	ld de, BattleBG_Map
	call ParseMap				; Battle BG
	call MakeBox

; set intial SCX
	ld a, $A0
	ld [v_Scrollx], a
	ld a, $60
	ld [v_Scrollx2], a
	call EnableLCD
	call GBFadeInFromWhite
	
; make partial Mishu backsprite in OAM -- in the style of Pokemon
	;ld hl, a_Sprites
	;ld de, Data_OAM_2
	;ld bc, Data_OAM_2_End - Data_OAM_2
	;call CopyRAM							; Backsprite OAM data
	ld hl, a_Sprites
	ld de, $38A6
	ld bc, (2 * $100) + 7
	ld a, $A0
	call GenerateOAM
; make the rest of the backsprite in BGMap
	ld hl, vBGMap0+1+($20*8)
	ld a, $AE
	ld de, $0407	;rows, columns
	call GenerateMap
	
; make Lumi
	ld hl, $9800 + $20 + 12
	ld a, $76
	ld de, $0706
	call GenerateMap
	
; ===========================================================================

.SceneLoop_0						; Scroll "players"
	ld a, [v_Scrollx2]
	inc a
	inc a							; v_Scrollx2 += 2, scroll right
	ld [v_Scrollx2],a
	ld a, [v_Scrollx]
	dec a
	dec a							; v_Scrollx =- 2, scroll left
	jr z, .dotext					; once v_Scrollx is in resting position
	ld [v_Scrollx], a

; apply scroll positions
	ld [rSCX], a

	waitY 64
	ld a, [v_Scrollx2]
	ld [rSCX], a
	
	waitY 96
	xor a
	ld [rSCX], a
	
	ld a, 2*7						; number of entries to change
	ld hl, a_Sprites+1
.moveOAM
	dec [hl]
	dec [hl]						; offset X position of entry by -2
	inc hl
	inc hl
	inc hl
	inc hl							; move to next OAM entry
	dec a
	jr nz, .moveOAM
	
	call DelayFrame
	jr .SceneLoop_0
	

.dotext
; make health bars
	ld hl, vBGMap0
	ld de, HP_Map
	call ParseMap					; create status bars
	
	ld hl, m_HP_Enemy				; BG location
	ld de, v_HP_Enemy				; Variable pointer
	call DrawHP
	
	ld hl, m_HP_Player
	ld de, v_HP_Player
	call DrawHP
	
; bake sprites in map
	ld hl, vBGMap0+$C1
	ld a, $A0
	ld de, $0207					;row, column
	call GenerateMap
	call ClearSprites
	
	ld de, Dialog_Battle1
	call MakeDialogText
	ld b, 4							; blink 4 times
	call BlinkArrow_
;	xor a

	ld de, Dialog_Battle2
	call MakeDialogText
	ld b, 2
	call BlinkArrow_
	
	ld de, Dialog_Battle4
	call MakeDialogText
	
	ld c, 20
	call DelayFrames
	
	ld de, Dialog_Battle5
	call MakeDialogText
	
; Make battle FX
; 2x stars

	ld a, $5D
	ld hl, a_Sprites
	ld bc, $0303
	ld d, 40+16
	ld e, 6
	call GenerateOAM
	
	ld a, $5D
	ld hl, a_Sprites + (3*3*4)
	ld bc, $0303
	ld d, 64+16
	ld e, 40
	call GenerateOAM
	
	call EnemyBump
	call ClearSprites
	call BlinkPlayer
	
.animateHPloss
	ld a, [v_HP_Player]
	and a
	jr z, .fainted
	dec a
	ld [v_HP_Player], a
	call UpdateHP
	call DelayFrame
	call DelayFrame
	jr .animateHPloss
.fainted
	ld hl, $98C1
	ld bc, $0607
	call FillArea					; clear player
	call GBFadeInFromWhite
	
	ld de, Dialog_Battle6
	call MakeDialogText
	
	ld b, 2
	call BlinkArrow_

.training
	call DisableLCD
	
	; make new thingies
	ld a, "2"
	ld hl, $9828
	ld [hl], a				; Lumi levelled up to lv.32
	ld hl, $9930
	ld [hli], a
	ld a, "8"
	ld [hl], a				; Mishu magically levelled up to lv.28
	ld a, 12				; max hp again
	ld [v_HP_Player], a
	ld hl, m_HP_Player
	ld de, v_HP_Player
	call DrawHP
	
	; regenerate player tmaps
	ld hl, vBGMap0+1+($20*6)
	ld a, $A0
	ld de, $0607
	call GenerateMap
	
	; save data for later
	ld hl, $C100
	ld de, vChars0 + $600
	ld bc, $E70 - $600
	call CopyRAM
	ld hl, $C100 + $E70 - $600 + 1
	ld de, vBGMap0
	ld bc, $A40 - $800
	call CopyRAM
	
	call ClearScreen
	
	; copy tiles
	ld hl, vChars0 + $600
	ld de, GFX10
	ld bc, GFX10_End - GFX10
	call CopyRAM
	
	; copy map
	ld hl, vBGMap0-1
	ld de, Sleepy_Map
	call ParseMap
	
	call EnableLCD
	
	ld de, Sleepy_Text
	ld hl, vBGMap0 + $20 + 2
	call ParseText
	
	ld b, 6					; how many times to Z
.l
	; ???
	call WaitVRAM
	ld a, $E6
	ld hl,$990e
	ld [hl], a
	inc a
	ld hl,$992f
	ld [hl], a
	call WaitVRAM
	ld hl, $99C7
	ld a, $B7
	ld [hli], a
	inc a
	ld [hli], a
	inc a
	ld [hl], a
	call WaitVRAM
	ld hl, $99E7
	ld a, $C4
	ld [hli], a
	inc a
	ld [hli], a
	inc a
	ld [hl], a
	ld c, 20
	call DelayFrames
	call WaitVRAM
	ld a, $FF
	ld hl,$990e
	ld [hl], a
	ld hl,$992f
	ld [hl], a
	call WaitVRAM
	ld hl, $99C7
	ld a, $e8
	ld [hli], a
	inc a
	ld [hli], a
	inc a
	ld [hl], a
	call WaitVRAM
	ld hl, $99E7
	ld a, $eb
	ld [hli], a
	inc a
	ld [hli], a
	inc a
	ld [hl], a
	ld c, 20
	call DelayFrames
	dec b
	jr nz, .l

.end
	; load everything back
	call DisableLCD
	ld de, $C100
	ld hl, vChars0 + $600
	ld bc, $E70 - $600
	call CopyRAM
	ld de, $C100 + $E70 - $600 + 1
	ld hl, vBGMap0
	ld bc, $A40 - $800
	call CopyRAM
	call EnableLCD
	
	ld de, Dialog_Battle7
	call MakeDialogText
	call ShakeScreen
	call BlinkEnemy
	ld hl, v_HP_Enemy
	dec [hl]
	call UpdateHP
	
	ld de, Dialog_Battle8
	call MakeDialogText
	
; water gun
	ld a, $5B
	ld hl, a_Sprites
	ld bc, $0201
	ld d, 40
	ld e, 6
	call GenerateOAM
	ld a, $5B
	ld hl, a_Sprites + (2*4)
	ld bc, $0201
	ld d, 64
	ld e, 40
	call GenerateOAM
	call DelayFrame
	call DelayFrame
									; move water sprites
	ld b, 12	; # frames
	ld c, 3	; px to move
	ld d, 4		; many entries
.move
	ld hl, a_Sprites
.move2
	ld a, [hl]
	add a, c
	ld [hl], a
	inc hl
	inc hl
	inc hl
	inc hl
	dec d
	jr nz, .move2
	call DelayFrame
	ld d, 4
	dec b
	jr nz, .move
	
	call ClearSprites
	call BlinkPlayer
	ld hl, v_HP_Player
	dec [hl]
	dec [hl]
	dec [hl]
	dec [hl]
	call UpdateHP
	
	ld c, 10
	call DelayFrames
	
; suddenly Mishu gets to choose
	ld hl, $99C1
	ld bc, $0312
	call FillArea
	ld hl, $9d88-$400
	call MakeColumn
	ld hl, $9DCA-$400
	ld de, Text_BattleSelection
	call ParseText
	call WaitVRAM
	ld hl, $9DC9-$400
	ld a, $1E
	ld [hl], a
	
	ld c, 16
	call DelayFrames

	ld hl, $99C1
	ld bc, $0312
	call FillArea
	ld hl, $9d84-$400
	call MakeColumn
	ld hl, $9DA6-$400
	ld de, Text_MoveSelection
	call ParseText
	ld hl, $9988
	call ClearColumnParts
; "selection"
	ld bc, $0020
	call WaitVRAM
	ld a, $1E
	ld hl,$99A5
	ld [hl],a
	call Delay3
	call WaitVRAM
	xor a
	ld [hl], a	; clear this part
	ld a, $1E
	add hl,bc
	ld [hl],a	; select the next choice
	call Delay3
	call WaitVRAM
	xor a
	ld [hl], a
	ld a, $1E
	add hl,bc
	ld [hl],a
	call Delay3
	
	ld c, 25
	call DelayFrames
	
	ld hl, $9984
	call ClearColumnParts
	
	ld de, Dialog_Battle9
	call MakeDialogText
	ld c, 17
	call DelayFrames
	call DoWeirdScreen
	call ShakeScreen
	call BlinkEnemy
	
.animateHPloss2
	ld a, [v_HP_Enemy]
	and a
	jr z, .fainted2
	dec a
	ld [v_HP_Enemy], a
	call UpdateHP
	call DelayFrame
	call DelayFrame
	jr .animateHPloss2
.fainted2
	ld de, Dialog_Battle10
	call MakeDialogText
	
	ld c, 17
	call DelayFrames
	
	ld a, $FF
	ld hl, $9800 + $20 + 12
	ld bc, $0706
	call FillArea					; clear enemy
	call GBFadeInFromWhite
	
	ld de, Dialog_Battle11
	call MakeDialogText
	
	ld b, 2
	call BlinkArrow_
	
	ld de, Dialog_Battle12
	call MakeDialogText

; ===========================================================================

.x
	halt
	call CheckIfSceneIsDone
	jr nc, .x
	ld c, 24
	call DelayFrames
	jp JumpToNewScene 

; ===========================================================================
; Scene Functions
; ===========================================================================

MakeDialogText:
; Pops a dialog in a fixed location
; de = text location
	push de
	ld a, $FF
	ld hl, $99C1-$20
	ld bc, $0412
	call FillArea
	pop de
	ld hl, vBGMap0 + $1C1
	jp TypeText
	
ShakeScreen:
; Animates a vertical screen shake
; No parameters
	ld b, 4
.a
;	ld a, [rBGP]
;	cpl
;	ld [rBGP], a			; more seizures
	ld a, b
	ld [rSCY], a
	call DelayFrame
	call DelayFrame
	ld a, b
	cpl
	ld [rSCY], a
	call DelayFrame
	call DelayFrame
	dec b
	jp nz, .a
	ld a, 0
	ld [rSCY], a
	ret
	
DrawHP:
; Draws a bar indicating a level from 0-12 to the screen
; hl = VRAM location
; de = variable location
	ld a, $0a	; HP empty tile
	ld c, 6		; # levels
	push hl
.fill
	push af
	call WaitVRAM
	pop af
	ld [hli], a
	dec c
	jr nz, .fill
	pop hl
	ld a, [de]	; read HP
	and a
	ret z		; return if = 0
	ld c, a
.opop
; fills a tile with half level
; then full level
; then repeats until a correct
; representation is reached
	ld a, $1C	; half level
	push af
	call WaitVRAM
	pop af
	ld [hl], a
	dec c
	ret z
	ld a, $1D	; full level
	push af
	call WaitVRAM
	pop af
	ld [hl], a
	dec c
	ret z
	inc hl
	jr .opop
	
UpdateHP:
	ld hl, m_HP_Player
	ld de, v_HP_Player
	call DrawHP
	ld hl, m_HP_Enemy
	ld de, v_HP_Enemy
	jp DrawHP
	
BlinkArrow_:
	ld hl, $9A12	; hardcoded position
	jp BlinkArrow
	
MakeBox:
; Dialog box filth
; top side
	ld a, $14					; Top left
	ld [vBGMap0 + $180], a
	inc a
	ld hl, vBGMap0 + $180 + 1
	ld bc, 18
	call FillRAM				; Top line
	inc a
	ld [hl], a					; Top right
	ld hl, vBGMap0 + $1A0
; left and right sides
	ld d, 4
.ech
	ld bc, $13
	ld a, $18
	ld [hl], a
	add hl, bc
	ld [hl], a
	ld bc, $D
	add hl, bc
	dec d
	jr nz, .ech
; bottom side
	ld a, $17
	ld [vBGMap0 + $220], a
	ld a, $15
	ld hl, vBGMap0 + $220 + 1
	ld bc, 18
	call FillRAM
	ld a, $19
	ld [hl], a
	ret
	
MakeColumn:
; Make vertical side
; making it look like
; two boxes
; clobbers bc
	call WaitVRAM
	ld bc, $20
	ld a, $14
	ld [hl], a
; unrolled
	call WaitVRAM
	ld a, $18
	add hl, bc
	ld [hl], a
	call WaitVRAM
	ld a, $18
	add hl, bc
	ld [hl], a
	call WaitVRAM
	ld a, $18
	add hl, bc
	ld [hl], a
	call WaitVRAM
	ld a, $18
	add hl, bc
	ld [hl], a
	call WaitVRAM
	ld a, $17
	add hl, bc
	ld [hl], a
	ret
	
ClearColumnParts:
	ld bc, $20 * 5
	call WaitVRAM
	ld a, $15
	ld [hl], a
	add hl, bc
	ld [hl], a
	ret
	
BlinkEnemy:
	ld c, 3
.a
	push bc
	ld a, $FF
	ld hl, $9800 + $20 + 12
	ld bc, $0706
	call FillArea
	call Delay6
	ld hl, $9800 + $20 + 12
	ld a, $76
	ld de, $0706
	call GenerateMap
	call Delay6
	pop bc
	dec c
	ret z
	jr .a
	
BlinkPlayer:
	ld c, 3
.a
	push bc
	ld a, $FF
	ld hl,vBGMap0+1+($20*6)
	ld bc, $0607
	call FillArea
	call Delay6
	ld hl, vBGMap0+1+($20*6)
	ld a, $A0
	ld de, $0607
	call GenerateMap
	call Delay6
	pop bc
	dec c
	ret z
	jr .a
	
PlayerBump:
	ld a, $FF
	ld hl,vBGMap0+1+($20*6)
	ld bc, $0607
	call FillArea
	ld hl, vBGMap0+1+($20*6)+1
	ld a, $A0
	ld de, $0607
	call GenerateMap
	call Delay6
	ld a, $FF
	ld hl,vBGMap0+1+($20*6)+1
	ld bc, $0607
	call FillArea
	ld hl, vBGMap0+1+($20*6)
	ld a, $A0
	ld de, $0607
	call GenerateMap
	jp ShakeScreen
	
EnemyBump:
	ld a, $FF
	ld hl, $9800 + $20 + 12
	ld bc, $0706
	call FillArea
	ld hl, $9800 + $20 + 12 - 1
	ld a, $76
	ld de, $0706
	call GenerateMap
	call Delay6
	ld a, $FF
	ld hl, $9800 + $20 + 12 - 1
	ld bc, $0706
	call FillArea
	ld hl, $9800 + $20 + 12
	ld a, $76
	ld de, $0706
	call GenerateMap
	call WaitVRAM
	ld a, 4
	ld [$982b], a
	call Delay6
	jp ShakeScreen
	
DoWeirdScreen:
; sound driver overflows
; to LY=03... damn
	ld a, [rBGP]
	cpl
	ld [rBGP], a
	ld b, 0					; base frame counter
	ld c, 45				; how many frames
.x
	push bc
	ld d, 4					; line no. just to be safe
	ld e, 140				; how many lines
.y	
	call CheckLine
	ld hl, v_SineCounter
	ld a, [hl]
	inc a
	cp a, $20
	jr c, .z
	xor a
.z
	ld [hl], a
	call GetSine
	push af
	call WaitVRAM
	pop af
	ld [rSCX], a
	inc d
	dec e
	jr nz, .y
	pop bc
	inc b
	ld a, b
	cp a, $20
	jr c, .a
	xor a
.a
	ld b, a
	ld [v_SineCounter], a
	dec c
	jr nz, .x
	ld a, [rBGP]
	cpl
	ld [rBGP], a
	xor a
	ld [rSCX], a
	ret
